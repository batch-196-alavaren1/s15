console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:


let first = 'First Name:';
let fName = 'Clinton Vonn';
console.log(first, fName);

let last = 'Last Name:';
let lName = 'Alavaren';
console.log(last, lName);

let age = 29;
console.log("Age: " + age);

let hobby = 'Hobbies:';
console.log(hobby);
let hobbies = ["Eating", "Sleeping", "Watching Movies"];
console.log(hobbies);

let address = "Work Address:";
console.log(address);
let workAddress = {
	houseNumber: 578,
	street: 'Ventanilla',
	city: 'Pasay',
	state: 'Philippines'
}
console.log(workAddress);



/*

	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	

	let fullName = "Steve Rogers";
	console.log("My full name is" + fullName);

	let age1 = 40;
	console.log("My current age is: " + age1);

	let myFriends = "My Friends are:";
	console.log(myFriends);
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log(friends);

	let profile = "My Full Profile:";
	console.log(profile);

	let profile1 = {
		userName: "captain_america",
		fullName: "Steve Rogers",
		age: 40,
		isActive: false
	}
	console.log(profile1);

	let fullName1 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName1);

	let lastLocation = "Arctic Ocean";
	lastLocation = "Atlantic Ocean";
	console.log("I was found frozen in: " + lastLocation);